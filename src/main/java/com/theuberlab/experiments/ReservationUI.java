package com.theuberlab.experiments;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;

/**
 * ReservationUI 
 * 
 * @author Aaron Forster 
 * @date Feb 19, 2016
 */
@SpringUI(path = "ui")
@Theme("valo")
public class ReservationUI extends UI {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ReservationUI.class);

	/**
	 * The ReservationRepository to work with.
	 */
	private final ReservationRepository reservationRepository;
	/*
	 * ########################################################################
	 * Constructors
	 * ########################################################################
	 */

	@Autowired
	public ReservationUI(ReservationRepository reservationRepository) {
		this.reservationRepository = reservationRepository;
	}
	
	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */

	@Override
	protected void init(VaadinRequest request) {
		Grid table = new Grid();
		BeanItemContainer<Reservation> container = new BeanItemContainer<>(
				Reservation.class,
				this.reservationRepository.findAll());
		table.setContainerDataSource(container);
		table.setSizeFull();
		setContent(table);
	}
	/*
	 * ########################################################################
	 * Getters and Setters
	 * ########################################################################
	 */}
