package com.theuberlab.experiments;

import java.util.Collection;

import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * ReservationRepository 
 * 
 * @author Aaron Forster 
 * @date Feb 19, 2016
 */
@RepositoryRestResource
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
	final static org.slf4j.Logger logger = LoggerFactory.getLogger(ReservationRepository.class);

	@RestResource(path = "by-name", rel = "by-name")
	Collection<Reservation> findByReservationName(@Param("rn") String rn);
}