package com.theuberlab.experiments;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.slf4j.LoggerFactory;

/**
 * Reservation 
 * 
 * @author Aaron Forster 
 * @date Feb 19, 2016
 */
@Entity
public class Reservation {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(Reservation.class);
	
	/**
	 * The database Key.
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * The reservation name.
	 */
	private String reservationName;
	/*
	 * ########################################################################
	 * Constructors
	 * ########################################################################
	 */
	
	public Reservation(String reservationName) {
		this.reservationName = reservationName;
	}
	
	/**
	 * WTH is this? It's there from the demo. Why do we have an empty constructor?
	 */
	Reservation() {
	}
	
	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		//TODO: Peep these quotes and see if I can change the singles near the end to doubles.
		return "Reservation{" + "id=" + id + ", reservationName='" + reservationName + '\'' + '}';
	}
	
	/*
	 * ########################################################################
	 * Getters and Setters
	 * ########################################################################
	 */
	
	/**
	 * Get the reservation's ID.
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Get the reservation's name.
	 * 
	 * @return
	 */
	public String getReservationName() {
		return reservationName;
	}
}