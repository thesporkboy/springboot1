package com.theuberlab.experiments;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * ReservationMvcController 
 * 
 * @author Aaron Forster 
 * @date Feb 19, 2016
 */
@Controller
public class ReservationMvcController {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	/** A handle for the log4j logger factory. */
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ReservationMvcController.class);

	/**
	 * The ReservationRepository to work with.
	 */
	private final ReservationRepository reservationRepository;
	
	/*
	 * ########################################################################
	 * Constructors
	 * ########################################################################
	 */

	@Autowired
	public ReservationMvcController(ReservationRepository rr) {
		this.reservationRepository = rr;
	}
	
	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/reservations.mvc")
	public String renderReservations(Model model) {
		model.addAttribute("reservations", this.reservationRepository.findAll());
		// find template named 'reservations'
		return "reservations";
	}
	
	/*
	 * ########################################################################
	 * Getters and Setters
	 * ########################################################################
	 */
}
