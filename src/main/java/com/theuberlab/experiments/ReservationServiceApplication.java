package com.theuberlab.experiments;

import java.util.stream.Stream;

import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ReservationServiceApplication 
 * 
 * @author Aaron Forster 
 * @date Feb 19, 2016
 */
@Configuration
@SpringBootApplication
public class ReservationServiceApplication {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ReservationServiceApplication.class);

	/*
	 * ########################################################################
	 * Constructors
	 * ########################################################################
	 */

	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */

	//TODO: Update this stupid variable name.
	@Bean
	CommandLineRunner runner(ReservationRepository rr) {
		return args -> Stream.of("Julia", "Mia", "Phil", "Dave", "Pieter", "Bridget", "Stephane", "Josh", "Jennifer").forEach(n -> rr.save(new Reservation(n)));
	}
	
	@Bean
	HealthIndicator customHealthIndicator() {
		return new HealthIndicator() {
			@Override
			public Health health() {
				return Health.status("I <3 Java!").build();
			}
		};
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ReservationServiceApplication.class, args);
	}
	/*
	 * ########################################################################
	 * Getters and Setters
	 * ########################################################################
	 */
}
