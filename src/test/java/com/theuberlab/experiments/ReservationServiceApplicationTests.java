package com.theuberlab.experiments;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ReservationServiceApplication.class)
@WebAppConfiguration
public class ReservationServiceApplicationTests {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ReservationServiceApplication.class);

	private MediaType mediaType = MediaType.parseMediaType("application/hal+json");
	
	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Before
	public void before() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
	}

	@Test
	public void contextLoads() throws Exception {
		this.mockMvc.perform(
				get("/reservations")
				.accept(this.mediaType))
				.andExpect(content().
						contentType(this.mediaType))
				.andExpect(status().isOk());
	}
	
	/**
	 * Requests a reqource which does not exist.
	 * 
	 * @throws Exception
	 */
	@Test
	public void negativeTest() throws Exception {
		MediaType textMT = MediaType.parseMediaType("text/html");
		this.mockMvc.perform(
				get("/nothingatthispath")
				.accept(textMT))
				.andExpect(status().is4xxClientError());
	}
}